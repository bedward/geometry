import Point, { pointsAreNotEqual } from './point.js';

export const linesAreEqual = (line1, line2) => {
	if (pointsAreNotEqual(line1.start, line2.start)) return false;
	if (pointsAreNotEqual(line1.end, line2.end)) return false;
	return true;
}
export const linesAreNotEqual = (line1, line2) => !linesAreEqual(line1, line2);

export default class Line {
	constructor(start = new Point(), end = new Point()) {
		this.start = start;
		this.end = end;
	}

	static isType(line) {
		if (!line) return false;
		
		if (!Point.isType(line.start)) return false;
		if (!Point.isType(line.end)) return false;

		return true;
	}

	static from(obj) {
		if (!Line.isType(obj)) throw new TypeError('obj must be a Line');
		return new Line(Point.from(obj.start), Point.from(obj.end));
	}

	clone() {
		return new Line(this.start.clone(), this.end.clone());
	}

	get start() { return this.__start	}
	set start(start) {
		if (!Point.isType(start)) throw new TypeError('start must be a Point');
		this.__start = start;
	}

	get end() { return this.__end }
	set end(end) {
		if (!Point.isType(end)) throw new TypeError('end must be a Point');
		this.__end = end;
	}

	get length() {
		return Point.distance(this.start, this.end);
	}

	get delta() {
		return Point.sub(this.end, this.start);
	}

	get angle() {
		return this.delta.angle;
	}

	rotate(angle) {
		let center = this.delta.rotate(angle);

		this.end.x = this.start.x + center.x;
		this.end.y = this.start.y + center.y;

		return this;
	}

	rotateTo(angle) {
		this.rotate(-this.angle);
		this.rotate(angle);
	}

	//	incomplete
	// reflect(line) {
	// 	let out = { x: 0, y: 0 };

	// 	let vec = line.delta;
	// 	let norm = this.delta.normal;
	// 	let dot = this.dotProductOf(line.start);

	// 	out.x = line.x - 2 * this.dotProductOf()
	// }

	project(point) {
		if (!Point.isType(point)) throw new TypeError('point must be a Point');
		
		let dotCalc = (
			((point.x - this.start.x) * (this.end.x - this.start.x)) +
			((point.y - this.start.y) * (this.end.y - this.start.y))
		);

		let dotProduct = 0;
		if (dotCalc != 0)
			dotProduct = dotCalc / Math.pow(this.length, 2);

		return new Point(
			this.start.x + (dotProduct * (this.end.x - this.start.x)),
			this.start.y + (dotProduct * (this.end.y - this.start.y))
		);
	}
}