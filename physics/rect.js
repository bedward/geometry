import Point, { pointsAreNotEqual } from './point.js';
import Line from './line.js';

export const testRect = (rect, prop) => {
	if (!rect) return false;

	if (!Point.isType(rect.topLeft)) return false;
	if (!Point.isType(rect.topRight)) return false;
	if (!Point.isType(rect.bottomRight)) return false;
	if (!Point.isType(rect.bottomLeft)) return false;

	return true;
}

export const rectsAreEqual = (rect1, rect2) => {
	if (pointsAreNotEqual(rect1.topLeft, rect2.topLeft)) return false;
	if (pointsAreNotEqual(rect1.topRight, rect2.topRight)) return false;
	if (pointsAreNotEqual(rect1.bottomRight, rect2.bottomRight)) return false;
	if (pointsAreNotEqual(rect1.bottomLeft, rect2.bottomLeft)) return false;
	return true;
}
export const rectsAreNotEqual = (rect1, rect2) => !rectsAreEqual(rect1, rect2);


const sortClosestTo = {
	lowestX: (points) => points.sort((p1, p2) => {
		if (p1.x < p2.x) return -1;
		if (p1.x > p2.x) return 1;
		return 0;
	}),

	highestX: (points) => points.sort((p1, p2) => {
		if (p1.x < p2.x) return 1;
		if (p1.x > p2.x) return -1;
		return 0;
	}),
	
	lowestY: (points) => points.sort((p1, p2) => {
		if (p1.y < p2.y) return -1;
		if (p1.y > p2.y) return 1;
		return 0;
	}),

	highestY: (points) => points.sort((p1, p2) => {
		if (p1.y < p2.y) return 1;
		if (p1.y > p2.y) return -1;
		return 0;
	})
}

export default class Rect {
	constructor(
		topLeft = new Point(0, 0),
		topRight = new Point(1, 0),
		bottomRight = new Point(1, 1),
		bottomLeft = new Point(0, 1)
	) {
		this.topLeft = topLeft;
		this.topRight = topRight;
		this.bottomRight = bottomRight;
		this.bottomLeft = bottomLeft;
	}

	static isType(rect) {
		if (!rect) return false;
		
		if (!Point.isType(rect.topLeft)) return false;
		if (!Point.isType(rect.topRight)) return false;
		if (!Point.isType(rect.bottomRight)) return false;
		if (!Point.isType(rect.bottomLeft)) return false;

		return true;
	}

	get topLeft() { return this.__topLeft }
	set topLeft(point) {
		if (!Point.isType(point)) throw new TypeError('topLeft must be a Point');
		this.__topLeft = point;
	}

	get topRight() { return this.__topRight }
	set topRight(point) {
		if (!Point.isType(point)) throw new TypeError('topRight must be a Point');
		this.__topRight = point;
	}

	get bottomRight() { return this.__bottomRight }
	set bottomRight(point) {
		if (!Point.isType(point)) throw new TypeError('bottomRight must be a Point');
		this.__bottomRight = point;
	}

	get bottomLeft() { return this.__bottomLeft }
	set bottomLeft(point) {
		if (!Point.isType(point)) throw new TypeError('bottomLeft must be a Point');
		this.__bottomLeft = point;
	}


	get topLine() {
		return new Line(this.topLeft, this.topRight);
	}

	get rightLine() {
		return new Line(this.topRight, this.bottomRight);
	}

	get bottomLine() {
		return new Line(this.bottomRight, this.bottomLeft);
	}

	get leftLine() {
		return new Line(this.bottomLeft, this.topLeft);
	}

	get __points() {
		return [
			this.topLeft,
			this.topRight,
			this.bottomRight,
			this.bottomLeft
		]
	}

	get leftBound() {
		return sortClosestTo.lowestX(this.__points)[0].x;
	}

	get rightBound() {
		return sortClosestTo.highestX(this.__points)[0].x;
	}

	get topBound() {
		return sortClosestTo.lowestY(this.__points)[0].y;
	}

	get bottomBound() {
		return sortClosestTo.highestY(this.__points)[0].y;
	}


	get center() {
		return this.topLeft.clone()
			.add(this.topRight)
			.add(this.bottomRight)
			.add(this.bottomLeft)
			.divide(4);
	}

	get angle() {
		return this.bottomLine.delta.multi(.5)
			.add(this.bottomLine.start)
			.sub(this.center)
			.angle;
	}

	clone() {
		return new Rect(
			this.topLeft.clone(),
			this.topRight.clone(),
			this.bottomRight.clone(),
			this.bottomLeft.clone()
		);
	}

	add(offset) {
		if (!Point.isType(offset)) throw new Error('offset must be a Point');

		this.topLeft.add(offset);
		this.topRight.add(offset);
		this.bottomRight.add(offset);
		this.bottomLeft.add(offset);

		return this;
	}

	rotate(angle) {
		let center = this.center;

		let np1 = this.topLeft.clone().sub(center);
		np1.rotate(angle);

		let np2 = this.topRight.clone().sub(center);
		np2.rotate(angle);

		let np3 = this.bottomRight.clone().sub(center);
		np3.rotate(angle);

		let np4 = this.bottomLeft.clone().sub(center);
		np4.rotate(angle);

		this.topLeft = np1.add(center);
		this.topRight = np2.add(center);
		this.bottomRight = np3.add(center);
		this.bottomLeft = np4.add(center);

		return this;
	}

	rotateTo(angle) {
		this.rotate(-this.angle);
		this.rotate(angle);

		return this;
	}

	static fromPosSize(x, y, w, h) {
		return new Rect(
			new Point(x, y),
			new Point(x + w, y),
			new Point(x + w, y + h),
			new Point(x, y + h)
		)
	}

	static fromWidthHeight(pos, size) {
		return Rect.fromPosSize(
			pos.x,
			pos.y,
			size.x,
			size.y
		)
	}

	static fromLine(line) {
		return Rect.fromPosSize(
			line.start.x,
			line.start.y,
			line.delta.x,
			line.delta.y
		)
	}

	static asLine(rect) {
		testRect(rect, 'rect');

		return new Line(
			rect.topLeft.clone(),
			rect.bottomRight.clone()
		)
	}
}