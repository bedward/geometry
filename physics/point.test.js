import { file } from '@acce/tester';
import Point, { rad2deg, deg2rad, pointsAreNotEqual } from './point.js';

file('physics-point', async (unit) => {

	unit('physics point helpers', async (test) => {
		test('convert degrees to radians', () => {
			if (deg2rad(90) != 1.5707963267948966) throw 'not calculating radians correctly';
		});

		test('convert radians to degrees', () => {
			if (rad2deg(2) != 114.59155902616465) throw 'not calculating degrees correctly';
		});
	});

	unit('physics point class', async (test) => {
		test('new keyword creates an instance of Point', () => {
			let p = new Point();
			if (!(p instanceof Point)) throw 'did not create an instance of point';
		});

		test('point from object is instance of point', () => {
			let proto = { x: 0, y: 0 };
			let p = Point.from(proto);
			if (!(p instanceof Point)) throw 'did not create an instance of point';
		});
	});

	unit('phsics point constructor behaviour', async (test) => {
		test('default values when initialised', () => {
			let point = new Point();

			if (point.x != 0) throw 'default value is not 0';
			if (point.y != 0) throw 'default value is not 0';
		});

		test('non-default valueswhen initialised', () => {
			let point = new Point(10, 20);

			if (point.x != 10) throw 'x value is not 10';
			if (point.y != 20) throw 'y value is not 20';
		});

		test('change values after initialise', () => {
			let point = new Point();

			point.x = 10;
			point.y = 20;

			if (point.x != 10) throw 'x value is not 10';
			if (point.y != 20) throw 'y value is not 20';
		});

		test('cloning points creates identical values', () => {
			let point = new Point(10, 20);
			let clone = point.clone();

			if (point == clone) throw 'clone is not unique';
			if (point.x != clone.x) throw 'x value was not cloned correctly';
			if (point.y != clone.y) throw 'y value was not cloned correctly';
		});
	});

	unit('physics point properties', async (test) => {
		test('point length', () => {
			let point = new Point(10, 20);

			if (point.length !== 22.360679774997898) throw 'not calculating correctly';
		});

		test('point angle', () => {
			let point = new Point(10, 20);

			if (point.angle !== 26.56505117707799) throw 'not calculating correctly';
		});

		test('point normal', () => {
			let point = new Point(10, 20);
			let actual = point.normal;
			let expected = {
				x: 0.4472135954999579,
				y: 0.8944271909999159
			}

			if (pointsAreNotEqual(actual, expected)) throw 'not calculating correctly';
		});

		test('point left', () => {
			let point = new Point(10, 20);
			let actual = point.left;
			let expected = {
				x: -10,
				y: 20
			}

			if (pointsAreNotEqual(actual, expected)) throw 'not calculating correctly';
		});

		test('point right', () => {
			let point = new Point(10, 20);
			let actual = point.right;
			let expected = {
				x: 10,
				y: -20
			}

			if (pointsAreNotEqual(actual, expected)) throw 'not calculating correctly';
		});

		test('point reverse', () => {
			let point = new Point(10, 20);
			let actual = point.reverse;
			let expected = {
				x: -10,
				y: -20
			}

			if (pointsAreNotEqual(actual, expected)) throw 'not calculating correctly';
		});
	});

	unit('physics point methods', async (test) => {
		test('point.proto.add', () => {
			let p1 = new Point(10, 20);
			let p2 = new Point(30, 40);

			let actual = p1.add(p2);
			let expected = {
				x: 40,
				y: 60
			}

			if (pointsAreNotEqual(actual, expected)) throw '';
		});

		test('point.proto.sub', () => {
			let p1 = new Point(10, 20);
			let p2 = new Point(30, 40);

			let actual = p1.sub(p2);
			let expected = {
				x: -20,
				y: -20
			}

			if (pointsAreNotEqual(actual, expected)) throw '';
		});

		test('point.proto.divide', () => {
			let p1 = new Point(10, 20);

			let actual = p1.divide(2);
			let expected = {
				x: 5,
				y: 10
			}

			if (pointsAreNotEqual(actual, expected)) throw '';
		});

		test('point.proto.multi', () => {
			let p1 = new Point(10, 20);

			let actual = p1.multi(.5);
			let expected = {
				x: 5,
				y: 10
			}

			if (pointsAreNotEqual(actual, expected)) throw '';
		});

		test('point.proto.distance', () => {
			let p1 = new Point(10, 20);
			let p2 = new Point(30, 40);

			if (p1.distance(p2) != 28.284271247461902) throw '';
		});

		test('point.proto.rotate', () => {
			let p1 = new Point(1, 1);
			p1.rotate(90);

			if (p1.angle != 135) throw 'Angles are not applying correctly';
		});

		test('point.proto.rotateTo', () => {
			let p1 = new Point(10, 20);
			let actual = p1.rotateTo(90);

			if (actual.angle != 90) throw 'Angles are not being set correctly';
		});
	});

});