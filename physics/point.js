export const rad2deg = radians => radians * (180 / Math.PI);
export const deg2rad = degrees => degrees * (Math.PI / 180);

export const pointsAreEqual = (p1, p2) => {
	if (p1.x != p2.x) return false;
	if (p1.y != p2.y) return false;
	return true;
}
export const pointsAreNotEqual = (p1, p2) => !pointsAreEqual(p1, p2);

export default class Point {
	constructor(x = 0, y = 0) {
		this.x = x;
		this.y = y;
	}

	static isType(point) {
		if (!point) return false;
		
		//	no false positives for Balls
		if (point.radius && typeof (point.radius) == 'number') return false;

		if (typeof point.x != 'number') return false;
		if (typeof point.y != 'number') return false;

		return true;
	}

	get x() { return this._x }
	set x(num) {
		if (typeof num != 'number') throw new TypeError('x must be a number');
		this._x = num;
	}

	get y() { return this._y }
	set y(num) {
		if (typeof num != 'number') throw new TypeError('y must be a number');
		this._y = num;
	}

	get length() {
		return Math.sqrt(
			(this.x * this.x) +
			(this.y * this.y)
		);
	}

	get normal() {
		let nX = this.x == 0 || this.length == 0 ? 0 : this.x / this.length;
		let nY = this.y == 0 || this.length == 0 ? 0 : this.y / this.length;

		return new Point(nX, nY);
	}

	get angle() {
		return rad2deg(Math.atan2(this.x, this.y));
	}


	get left() {
		return new Point(this.x * -1, this.y);
	}
	get right() {
		return new Point(this.x, this.y * -1);
	}

	get reverse() {
		return new Point(this.x * -1, this.y * -1);
	}


	limit(limit) {
		if (typeof limit != 'number') throw new TypeError('limit must be a number');

		if (this.length > limit) {
			let nPoint = Point.limit(this, limit);
			this.x = nPoint.x;
			this.y = nPoint.y;
		}

		return this;
	}

	static limit(point, limit) {
		if (!Point.isType(point)) throw new TypeError('point must be a Point');
		if (typeof limit != 'number') throw new TypeError('limit must be a number');

		return point.length > limit ?
			Point.multi(point.normal, limit) :
			point;
	}

	clone() {
		return Point.from(this);
	}

	static from(obj) {
		if (!Point.isType(obj)) throw new TypeError('obj must be a Point');
		return new Point(obj.x, obj.y);
	}

	static simple(obj) {
		if (!Point.isType(obj) || !(obj instanceof Point))
			throw new TypeError('obj must be an instance of Point');

		return { x: obj.x, y: obj.y };
	}


	add(point) {
		let nPoint = Point.add(this, point);
		this.x = nPoint.x;
		this.y = nPoint.y;

		return this;
	}

	static add(p1, p2) {
		if (!Point.isType(p1)) throw new TypeError('p1 must be a Point');
		if (!Point.isType(p2)) throw new TypeError('p2 must be a Point');

		return new Point(p1.x + p2.x, p1.y + p2.y);
	}


	sub(point) {
		let nPoint = Point.sub(this, point);
		this.x = nPoint.x;
		this.y = nPoint.y;

		return this;
	}

	static sub(p1, p2) {
		if (!Point.isType(p1)) throw new TypeError('p1 must be a Point');
		if (!Point.isType(p2)) throw new TypeError('p2 must be a Point');

		return new Point(p1.x - p2.x, p1.y - p2.y);
	}


	divide(magnitude) {
		if (typeof magnitude != 'number') throw new TypeError('magnitude must be a number');

		let nPoint = Point.divide(this, magnitude);
		this.x = nPoint.x;
		this.y = nPoint.y;

		return this;
	}

	static divide(p1, magnitude) {
		if (!Point.isType(p1)) throw new TypeError('p1 must be a Point');
		if (typeof magnitude != 'number') throw new TypeError('magnitude must be a number');

		return new Point(p1.x / magnitude, p1.y / magnitude);
	}


	multi(magnitude) {
		if (typeof magnitude != 'number') throw new TypeError('magnitude must be a number');

		let nPoint = Point.multi(this, magnitude);
		this.x = nPoint.x;
		this.y = nPoint.y;

		return this;
	}

	static multi(p1, magnitude) {
		if (!Point.isType(p1)) throw new TypeError('p1 must be a Point');
		if (typeof magnitude != 'number') throw new TypeError('magnitude must be a number');

		let sum = { x: p1.x, y: p1.y };
		if (p1.x !== 0) sum.x *= magnitude;
		if (p1.y !== 0) sum.y *= magnitude;
		return new Point(sum.x, sum.y);
	}


	distance(point) {
		if (!Point.isType(point)) throw new TypeError('point must be a Point');

		return Point.distance(this, point);
	}

	static distance(p1, p2) {
		if (!Point.isType(p1)) throw new TypeError('p1 must be a Point');
		if (!Point.isType(p2)) throw new TypeError('p2 must be a Point');

		if (p1.length > p2.length) {
			return Point.sub(p1, p2).length;
		} else {
			return Point.sub(p2, p1).length;
		}
	}


	rotate(angle) {
		if (typeof angle != 'number') throw new TypeError('angle must be a number');

		let nPoint = Point.rotate(this, angle);
		this.x = nPoint.x;
		this.y = nPoint.y;
		return this;
	}

	static rotate(point, angle) {
		if (!Point.isType(point)) throw new TypeError('point must be a Point');
		if (typeof angle != 'number') throw new TypeError('angle must be a number');

		let radians = deg2rad(angle);

		let cos = Math.cos(radians);
		let sin = Math.sin(radians);

		return new Point(
			(cos * point.x) + (sin * point.y),
			(cos * point.y) - (sin * point.x)
		);
	}

	rotateTo(angle) {
		if (typeof angle != 'number') throw new TypeError('angle must be a number');

		let nPoint = Point.setRotation(this, angle);
		this.x = nPoint.x;
		this.y = nPoint.y;

		return this;
	}

	static setRotation(point, angle) {
		if (!Point.isType(point)) throw new TypeError('point must be a Point');
		if (typeof angle != 'number') throw new TypeError('angle must be a number');

		let reset = Point.rotate(point, -point.angle);
		return reset.rotate(angle);
	}
}