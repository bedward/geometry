import { file } from '@acce/tester';
import Point from './point.js';
import Ball, { ballsAreNotEqual } from './ball.js';

file('physics-ball', async (unit) => {

	unit('physics ball constructor', async (test) => {
		test('new keyword', () => {
			let ball = new Ball();

			if (!(ball instanceof Ball)) throw 'not an instance of Ball';
		});

		test('from point', () => {
			let point = new Point();
			let ball = Ball.from(point);

			if (!(ball instanceof Ball)) throw 'not an instance of Ball';
		});
	});

	unit('physics ball initial values', async (test) => {
		test('default constructor', () => {
			let ball = new Ball();

			if (ball.x != 0) throw 'ball.x is not 0';
			if (ball.y != 0) throw 'ball.y is not 0';
			if (ball.radius != 25) throw 'ball.radius is not 25';
		});

		test('non-default constructor', () => {
			let ball = new Ball(10, 10, 125);

			if (ball.x != 10) throw 'ball.x is not 10';
			if (ball.y != 10) throw 'ball.y is not 10';
			if (ball.radius != 125) throw 'ball.radius is not 125';
		});

		test('from point default radius', () => {
			let ball = Ball.from(new Point());

			if (ball.x != 0) throw 'ball.x is not 0';
			if (ball.y != 0) throw 'ball.y is not 0';
			if (ball.radius != 25) throw 'ball.radius is not 25';
		});

		test('from point non-default radius', () => {
			let ball = Ball.from(new Point(10, 10), 125);

			if (ball.x != 10) throw 'ball.x is not 10';
			if (ball.y != 10) throw 'ball.y is not 10';
			if (ball.radius != 125) throw 'ball.radius is not 125';
		});

		test('changing values after construct', () => {
			let ball = new Ball(10, 10, 125);

			ball.x = 20;
			ball.y = 20;
			ball.radius = 12;

			if (ball.x != 20) throw 'ball.x is not 20';
			if (ball.y != 20) throw 'ball.y is not 20';
			if (ball.radius != 12) throw 'ball.radius is not 12';
		});
	});

	unit('physics ball properties', async (test) => {
		test('get asPoint', () => {
			let ball = new Ball();
			let point = ball.asPoint;

			if (!(point instanceof Point)) throw 'not a point';
			if (point instanceof Ball) throw 'its still a ball';
		});
	});

	unit('physics ball methods', async (test) => {
		test('clone', () => {
			let ball = new Ball(10, 10, 125);
			let clone = ball.clone();

			if (!(clone instanceof Ball)) throw 'its still a ball';
			if (ballsAreNotEqual(ball, clone)) throw 'the clone is not equal';
		});
	});

});