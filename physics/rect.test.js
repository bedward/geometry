import { file } from '@acce/tester';
import Rect, { rectsAreNotEqual } from './rect.js';
import Point, { pointsAreNotEqual } from './point.js';
import Line, { linesAreNotEqual } from './line.js';

file('physics-rect', async (unit) => {

	unit('physics rect constructor', async (test) => {
		let rect = new Rect(
			new Point(10, 10),
			new Point(20, 10),
			new Point(20, 20),
			new Point(10, 20)
		);

		test('new keyword', () => {
			if (!(rect instanceof Rect)) throw 'not constructing';
		});

		test('from x y w h values', () => {
			let from = Rect.fromPosSize(10, 10, 10, 10);

			if (!(from instanceof Rect)) throw 'not constructing';
			if (rectsAreNotEqual(rect, from)) throw 'not equivilent'
		});

		test('from width/height points', () => {
			let from = Rect.fromWidthHeight(
				new Point(10, 10),
				new Point(10, 10)
			);

			if (!(from instanceof Rect)) throw 'not constructing';
			if (rectsAreNotEqual(rect, from)) throw 'not equivilent'
		});

		test('from line', () => {
			let line = new Line(new Point(10, 10), new Point(20, 20));
			let from = Rect.fromLine(line);

			if (!(from instanceof Rect)) throw 'not constructing';
			if (rectsAreNotEqual(rect, from)) throw 'not equivilent'
		});

		test('back to line', () => {
			let line = new Line(new Point(10, 10), new Point(20, 20));
			let from = Rect.fromLine(line);
			let actual = Rect.asLine(from);

			if (linesAreNotEqual(actual, line)) throw 'not converting back to a line correctly';
		});

		test('from clone', () => {
			let from = rect.clone();

			if (!(from instanceof Rect)) throw 'not constructing';
			if (rectsAreNotEqual(rect, from)) throw 'not equivilent'
		});
	});

	unit('physics rect initial values', async (test) => {
		test('default values', () => {
			let rect = new Rect();
			let expected = {
				topLeft: { x: 0, y: 0 },
				topRight: { x: 1, y: 0 },
				bottomRight: { x: 1, y: 1 },
				bottomLeft: { x: 0, y: 1 }
			}

			if (rectsAreNotEqual(rect, expected)) throw 'defaults are not correct';
		});

		test('non-default values', () => {
			let rect = new Rect(
				new Point(10, 10),
				new Point(20, 10),
				new Point(20, 20),
				new Point(10, 20)
			);

			let expected = {
				topLeft: { x: 10, y: 10 },
				topRight: { x: 20, y: 10 },
				bottomRight: { x: 20, y: 20 },
				bottomLeft: { x: 10, y: 20 }
			}

			if (rectsAreNotEqual(rect, expected)) throw 'non-defaults are not correct';
		});

		test('values changed after init', () => {
			let rect = new Rect(
				new Point(10, 10),
				new Point(20, 10),
				new Point(20, 20),
				new Point(10, 20)
			);

			rect.topLeft = new Point(20, 20);
			rect.topRight = new Point(40, 20);
			rect.bottomRight = new Point(40, 40);
			rect.bottomLeft = new Point(20, 40);

			let expected = {
				topLeft: { x: 20, y: 20 },
				topRight: { x: 40, y: 20 },
				bottomRight: { x: 40, y: 40 },
				bottomLeft: { x: 20, y: 40 }
			}

			if (rectsAreNotEqual(rect, expected)) throw 'non-defaults are not correct';
		});
	});

	unit('physics rect properties', async (test) => {
		test('rect lines', () => {
			let rect = new Rect(
				new Point(10, 10),
				new Point(20, 10),
				new Point(20, 20),
				new Point(10, 20)
			);

			let line1 = rect.topLine;
			let expected1 = { start: { x: 10, y: 10 }, end: { x: 20, y: 10 } }
			if (linesAreNotEqual(line1, expected1)) throw 'line1 did not generate correctly';

			let line2 = rect.rightLine;
			let expected2 = { start: { x: 20, y: 10 }, end: { x: 20, y: 20 } }
			if (linesAreNotEqual(line2, expected2)) throw 'line2 did not generate correctly';

			let line3 = rect.bottomLine;
			let expected3 = { start: { x: 20, y: 20 }, end: { x: 10, y: 20 } }
			if (linesAreNotEqual(line3, expected3)) throw 'line3 did not generate correctly';

			let line4 = rect.leftLine;
			let expected4 = { start: { x: 10, y: 20 }, end: { x: 10, y: 10 } }
			if (linesAreNotEqual(line4, expected4)) throw 'line4 did not generate correctly';
		});

		test('rect bounds', () => {
			let rect = new Rect(
				new Point(10, 10),
				new Point(20, 10),
				new Point(20, 20),
				new Point(10, 20)
			);

			if (rect.leftBound != 10) throw 'leftBound was not correct';
			if (rect.rightBound != 20) throw 'rightBound was not correct';
			if (rect.topBound != 10) throw 'topBound was not correct';
			if (rect.bottomBound != 20) throw 'bottomBound was not correct';
		});

		test('rect center', () => {
			let rect = new Rect(
				new Point(10, 10),
				new Point(20, 10),
				new Point(20, 20),
				new Point(10, 20)
			);

			let expected = { x: 15, y: 15 }

			if (pointsAreNotEqual(rect.center, expected)) throw 'failed to create center point correctly';
		});

		test('rect angle', () => {
			let rect = new Rect(
				new Point(10, 10),
				new Point(20, 10),
				new Point(20, 20),
				new Point(10, 20)
			);

			if (rect.angle != 0) throw 'failed to find angle';
		});
	});

	unit('physics rect methods', async (test) => {
		test('add a positional offset to the whole rect', () => {
			let rect = new Rect(
				new Point(10, 10),
				new Point(20, 10),
				new Point(20, 20),
				new Point(10, 20)
			);

			rect.add(new Point(10, 10));

			let expected = {
				topLeft: { x: 20, y: 20 },
				topRight: { x: 30, y: 20 },
				bottomRight: { x: 30, y: 30 },
				bottomLeft: { x: 20, y: 30 }
			}

			if (rectsAreNotEqual(rect, expected)) throw 'offset for whole rect not working';
		});

		test('add a positional offset to the whole rect', () => {
			let rect = new Rect(
				new Point(10, 10),
				new Point(20, 10),
				new Point(20, 20),
				new Point(10, 20)
			);

			rect.add(new Point(10, 10));

			let expected = {
				topLeft: { x: 20, y: 20 },
				topRight: { x: 30, y: 20 },
				bottomRight: { x: 30, y: 30 },
				bottomLeft: { x: 20, y: 30 }
			}

			if (rectsAreNotEqual(rect, expected)) throw 'offset for whole rect not working';
		});

		test('rect rotate', () => {
			let rect = new Rect(
				new Point(10, 10),
				new Point(20, 10),
				new Point(20, 20),
				new Point(10, 20)
			);

			rect.rotate(45);

			const expected = {
				topLeft: { x: 7.9289321881345245, y: 15 },
				topRight: { x: 15, y: 7.9289321881345245 },
				bottomRight: { x: 22.071067811865476, y: 15 },
				bottomLeft: { x: 15, y: 22.071067811865476 },
			};

			if (rectsAreNotEqual(rect, expected)) throw 'rotations still not working';
			if (rect.angle != 45) throw 'rotations not calculated correctly for angle';
		});

		test('rect rotateTo', () => {
			let rect = new Rect(
				new Point(10, 10),
				new Point(20, 10),
				new Point(20, 20),
				new Point(10, 20)
			);

			rect.rotate(45);
			rect.rotateTo(90);

			const expected = {
				topLeft: { x: 10, y: 20 },
				topRight: { x: 10, y: 10 },
				bottomRight: { x: 20, y: 10 },
				bottomLeft: { x: 20, y: 20 },
			};

			if (rectsAreNotEqual(rect, expected)) throw 'rotations still not working';
			if (rect.angle != 90) throw 'rotations not calculated correctly for angle';
		});
	});

});