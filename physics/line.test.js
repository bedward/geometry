import { file } from '@acce/tester';
import Line from './line.js';
import Point, { pointsAreNotEqual } from './point.js';

file('physics-line', async (unit) => {

	unit('physics line class', async (test) => {
		test('new keyword creates an instance of Line', () => {
			let line = new Line();
			if (!(line instanceof Line)) throw 'did not create an instance of line';
		});

		test('point from object is instance of line', () => {
			let line = Line.from({
				start: { x: 10, y: 10 },
				end: { x: 20, y: 20 }
			});

			if (!(line instanceof Line)) throw 'did not create an instance of line';
		});
	});

	unit('physics line constructor behaviour', async (test) => {
		test('default values at init', () => {
			let line = new Line();

			if (!(line.start instanceof Point)) throw 'not creating default values';
			if (!(line.end instanceof Point)) throw 'not creating default values';
		});

		test('non-default values at init', () => {
			let line = new Line(new Point(10, 10), new Point(20, 20));

			let actual = line.delta;
			let expected = {
				x: 10,
				y: 10
			};

			if (pointsAreNotEqual(actual, expected)) throw 'values are not created correctly';
		});

		test('changed values after init', () => {
			let line = new Line(new Point(10, 10), new Point(20, 20));

			line.start = { x: 20, y: 5 };
			line.end = { x: 50, y: 25 };

			let actual = line.delta;
			let expected = {
				x: 30,
				y: 20
			};

			if (pointsAreNotEqual(actual, expected)) throw 'values are not changing correctly';
		});

		test('clone creates identical values', () => {
			let line = new Line(new Point(10, 10), new Point(20, 20));
			let clone = line.clone();

			if (pointsAreNotEqual(line.start, clone.start)) throw 'not cloning correctly';
			if (pointsAreNotEqual(line.end, clone.end)) throw 'not cloning correctly';
			if (line == clone) throw 'line was not cloned, just the points were';
		});
	});

	unit('physics line properties', async (test) => {
		test('line length', () => {
			let line = new Line(new Point(10, 10), new Point(20, 20));

			if (line.length != 14.142135623730951) throw 'not correct length';
		});

		test('line delta', () => {
			let line = new Line(new Point(10, 10), new Point(20, 20));
			let actual = line.delta;
			let expected = {
				x: 10,
				y: 10
			}

			if (pointsAreNotEqual(actual, expected)) throw 'error calculating delta';
		});

		test('line angle', () => {
			let line = new Line(new Point(10, 10), new Point(20, 20));
			if (line.angle != 45) throw 'angle miss calculates';
		});
	});

	unit('physics line methods', async (test) => {
		test('line rotate', () => {
			let line = new Line(new Point(10, 10), new Point(20, 20));
			line.rotate(90);

			if (line.angle != 135) throw 'rotate does not work';
		});

		test('line rotateTo', () => {
			let line = new Line(new Point(10, 10), new Point(20, 20));
			line.rotateTo(90);

			if (line.angle != 90) throw 'rotate does not work';
		});

		test('line project', () => {
			let line = new Line(
				new Point(10, 0),
				new Point(30, 10)
			);

			let point = new Point(25, 10);

			let actual = line.project(point);
			let expected = {
				x: 26,
				y: 7.999999999999999
			}

			if (pointsAreNotEqual(actual, expected)) throw 'could not project point on the line correctly';
		});

		// test('line reflect', () => {
		// 	let line = new Line(new Point(10, 10), new Point(20, 20));
		// 	if (line.angle != 45) throw 'angle miss calculates';
		// });
	});

});