import Point from './point.js';

export const ballsAreEqual = (ball1, ball2) => {
	if (ball1.x != ball2.x) return false;
	if (ball1.y != ball2.y) return false;
	if (ball1.radius != ball2.radius) return false;
	return true;
}
export const ballsAreNotEqual = (ball1, ball2) => !ballsAreEqual(ball1, ball2);

export default class Ball extends Point {
	static from(point, radius) {
		if (!Point.isType(point)) throw new TypeError('point must be a Point');

		return new Ball(point.x, point.y, radius);
	}

	constructor(x, y, radius = 25) {
		super(x, y);
		this.radius = radius;
	}

	static isType(ball) {
		if (!ball) return false;
		
		if (typeof ball.x != 'number') return false;
		if (typeof ball.y != 'number') return false;
		if (typeof ball.radius != 'number') return false;

		return true;
	}

	get radius() { return this.__radius }
	set radius(radius) {
		if (typeof radius != 'number') throw new TypeError('radius must be a number');
		this.__radius = radius;
	}

	clone() {
		return new Ball(this.x, this.y, this.radius);
	}

	get asPoint() {
		return new Point(this.x, this.y);
	}
}