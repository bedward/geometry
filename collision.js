import Point, { pointsAreEqual, pointsAreNotEqual } from './physics/point.js';
import Ball from './physics/ball.js';
import Line from './physics/line.js';
import Rect from './physics/rect.js';

import assert from 'assert';

const createCollisionEvent = (object1, object2) => {
	let collisions = new Set();
	let done = false;

	let collisionEvent = {
		objects: [object1, object2],
		collisions: []
	}

	let reservedNames = Reflect.ownKeys(collisionEvent);

	return {
		get add() {
			return (value, name) => {
				if (reservedNames.includes(name)) throw new Error(`'${name}' is a reserved name`);
				collisions.add([value, name]);
			}
		},
		get done() {
			return () => {
				if (done) throw new Error('this instance has already been completed');
				done = true;

				for (var [value, name] of collisions) {
					if (!!name) collisionEvent[name] = value;

					if (Point.isType(value) || Ball.isType(value))
						collisionEvent.collisions.push(value);
				}

				return collisionEvent;
			}
		}
	}
}

/*
 * returns the closest collision from a array,
 * otherwise returns an empty array
 */
export const getClosest = (location, collisions) => {
	if (!Point.isType(location)) throw new TypeError('target must be a Point');

	let hits = collisions
		.filter(collition => !!collition)
		.reduce((closest, collision) => {
			if (!closest) return collision;

			let closest_dist = Point.distance(closest, location);
			let hit_dist = Point.distance(collision, location);

			if (closest_dist <= hit_dist) return closest;
			return collision;
		}, undefined);

	if (!hits) return false;
	if (hits instanceof Array && hits.length <= 0) return false;

	try {
		testPoint(hits, 'hits');
		return hits;
	}
	catch {
		return false;
	}
}

/* 
 * detects a collision of a point and a point with a radius
 * returns true if occured, otherwise false
 */
export const pointBall = (point, ball) => {
	if (!Point.isType(point)) throw new TypeError('point must be a Point');
	if (!Ball.isType(ball)) throw new TypeError('ball must be a Ball');

	let dist = Point.distance(point, ball.asPoint) < ball.radius;
	if (!dist) return false;

	let collision = createCollisionEvent(point, ball);
	collision.add(point, 'dot');
	return collision.done();
}

/* 
 * detects a collision of a line and a point (with some buffer distance)
 * returns true if collision occured, otherwise false
 */
export const linePoint = (line, point, buffer = .1) => {
	if (!Line.isType(line)) throw new TypeError('line must be a Line');
	if (!Point.isType(point)) throw new TypeError('point must be a Point');

	let dist_start = Point.distance(point, line.start);
	let dist_end = Point.distance(point, line.end);
	let dist_sum = dist_start + dist_end;

	return dist_sum <= line.length + buffer && dist_sum <= line.length + buffer
}

/*
 * detects a collision of a line and a point with a radius
 * returns the point on the line if occured, or
 * returns false
 */
export const lineBall = (line, ball) => {
	if (!Line.isType(line)) throw new TypeError('line must be a Line');
	if (!Ball.isType(ball)) throw new TypeError('ball must be a Ball');

	let eventObject = createCollisionEvent(line, ball);

	//	records the angle of contact
	let hitAngle = line.delta.angle;
	eventObject.add(hitAngle, 'hitAngle');


	//	if the distance from the ball
	//	to the dot is smaller than the
	//	ball's radius, the extended line enters the ball
	let dot = line.project(ball.asPoint);
	if (pointBall(dot, ball)) {

		//	if the dot is also on the line,
		//	then we have a collision somewhere in the middle of the line
		if (linePoint(line, dot)) {
			eventObject.add(dot, 'dot');
		}

		let length = Point.distance(ball.asPoint, dot);
		let distanceToArc = Math.sqrt(ball.radius * ball.radius - length * length);

		let entry = line.start.clone().sub(dot).limit(distanceToArc).add(dot);
		let exit = line.end.clone().sub(dot).limit(distanceToArc).add(dot);

		//	only add tangent line points if it is both
		//	on the line AND inside the ball

		if (linePoint(line, entry)) eventObject.add(entry, 'entry');
		else if (pointBall(line.start, ball)) eventObject.add(line.start.clone(), 'entry');

		if (linePoint(line, exit)) eventObject.add(exit, 'exit');
		else if (pointBall(line.end, ball)) eventObject.add(line.end.clone(), 'exit');
	}

	let collision = eventObject.done();
	if (collision.collisions.length) return collision;
	return false;
}


/* 
 * detects a collision between two lines,
 * returns a point where the collision occured, or
 * returns false for no collision
 */
export const lineLine = (l1, l2) => {
	if (!Line.isType(l1)) throw new TypeError('l1 must be a Line');
	if (!Line.isType(l2)) throw new TypeError('l2 must be a Line');

	let uA = (
		(l2.end.x - l2.start.x) *
		(l1.start.y - l2.start.y) -
		(l2.end.y - l2.start.y) *
		(l1.start.x - l2.start.x)
	) / (
			(l2.end.y - l2.start.y) *
			(l1.end.x - l1.start.x) -
			(l2.end.x - l2.start.x) *
			(l1.end.y - l1.start.y)
		);

	let uB = (
		(l1.end.x - l1.start.x) *
		(l1.start.y - l2.start.y) -
		(l1.end.y - l1.start.y) *
		(l1.start.x - l2.start.x)
	) / (
			(l2.end.y - l2.start.y) *
			(l1.end.x - l1.start.x) -
			(l2.end.x - l2.start.x) *
			(l1.end.y - l1.start.y)
		);

	let intersect = (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1);
	if (!intersect) return false;

	let collisionEvent = createCollisionEvent(l1, l2);

	collisionEvent.add(l2.delta.angle, 'hitAngle');
	collisionEvent.add(new Point(
		l1.start.x + (uA * (l1.end.x - l1.start.x)),
		l1.start.y + (uA * (l1.end.y - l1.start.y))
	), 'dot');

	return collisionEvent.done();
}

/*
 * detects a collision between two balls
 * returns true when collisions occured, otherwise false
 */
export const ballBall = (ball1, ball2) => {
	if (!Ball.isType(ball1)) throw new TypeError('ball1 must be a Ball');
	if (!Ball.isType(ball2)) throw new TypeError('ball2 must be a Ball');

	let distance = Point.sub(ball1.asPoint, ball2.asPoint);

	if (distance.length <= ball1.radius + ball2.radius) {
		return distance.normal
			.multi(ball2.radius)
			.add(ball2.asPoint);
	}

	return false;
}


/*
 * detects a collision between a ball and a rectangle
 * returns the point that the collision occured, otherwise false
 */
export const rectBall = (rect, ball) => {
	if (!Rect.isType(rect)) throw new TypeError('rect must be a Rect');
	if (!Ball.isType(ball)) throw new TypeError('ball must be a Ball');

	let subtests = [
		lineBall(rect.topLine, ball),
		lineBall(rect.rightLine, ball),
		lineBall(rect.bottomLine, ball),
		lineBall(rect.leftLine, ball)
	].filter(test => !!test);

	let collisionEvent = createCollisionEvent(rect, ball).done();
	collisionEvent.collisions = subtests.map(sub => sub.collisions).flat(Infinity);

	let deduped = collisionEvent.collisions.reduce((deduped, item) => {
		if (!deduped.length) return [item];

		let shouldAdd = deduped.reduce((shouldAdd, dupe) => {
			if (!shouldAdd) return false;

			try {
				assert.deepStrictEqual(item, dupe);
				return false;
			}
			catch  { }

			return true;
		}, true);

		if (shouldAdd) return [...deduped, item];
		return deduped;
	}, []);

	collisionEvent.collisions = deduped;

	return collisionEvent;
}


/*
 * detects a collision between a ball and a line
 * returns an array of collisions, otherwise returns an empty array
 */
export const rectLine = (rect, line) => {
	if (!Rect.isType(rect)) throw new TypeError('rect must be a Rect');
	if (!Line.isType(line)) throw new TypeError('line must be a Line');

	let subtests = [
		lineLine(rect.topLine, line),
		lineLine(rect.rightLine, line),
		lineLine(rect.bottomLine, line),
		lineLine(rect.leftLine, line)
	].filter(collision => !!collision);

	let collisionEvent = createCollisionEvent(rect, line).done();

	collisionEvent.collisions = subtests.map(sub => sub.collisions).flat();

	return collisionEvent;
}


/*
 * returns the area of an object
 * for comparison against another
 */
export const physicsAreaOf = object => {
	if (Point.isType(object)) {
		return {
			x: object.x - 10,
			y: object.y - 10,
			w: object.x + 10,
			h: object.y + 10
		};
	}

	if (Ball.isType(object)) {
		return {
			x: object.x - object.radius - 10,
			y: object.y - object.radius - 10,
			w: object.x + object.radius + 10,
			h: object.y + object.radius + 10
		};
	}

	if (Line.isType(object)) {
		let lo_x = Math.min(object.start.x, object.end.x);
		let lo_y = Math.min(object.start.y, object.end.y);
		let hi_x = Math.max(object.start.x, object.end.x);
		let hi_y = Math.max(object.start.y, object.end.y);

		return {
			x: lo_x - 10,
			y: lo_y - 10,
			w: hi_x + 10,
			h: hi_y + 10
		};
	}

	if (Rect.isType(object)) {
		return {
			x: object.leftBound - 10,
			y: object.topBound - 10,
			w: object.rightBound + 10,
			h: object.bottomBound + 10
		};
	}
}

/*
 * Return a simple true/false
 * if the items are close enough together to test
 * more intense physics
 */
export const test = (object1, object2) => {
	object1 = physicsAreaOf(object1);
	object2 = physicsAreaOf(object2);

	//	object1 in area of object2
	if (object1.x > object2.w) return false;
	if (object1.w < object2.x) return false;
	if (object1.y > object2.h) return false;
	if (object1.h < object2.y) return false;

	return true;
}