import Point, { pointsAreNotEqual } from './physics/point.js';
import Line from './physics/line.js';
import Ball from './physics/ball.js';
import Rect from './physics/rect.js';

import { file } from '@acce/tester';
import { strict } from 'assert';
const assert = strict;

import { physicsAreaOf, pointBall, linePoint, ballBall, lineBall, lineLine, rectLine, rectBall } from './collision.js';


file('physics-collision', async (unit) => {

	unit('collision.physicsAreaOf', async (test) => {
		test('point', () => {
			let test = new Point(30, 30);

			let actual = physicsAreaOf(test);
			let expected = {
				x: 20,
				y: 20,
				h: 40,
				w: 40
			}

			assert.deepStrictEqual(actual, expected);
		});

		test('ball', () => {
			let test = new Ball(30, 30, 125);

			let actual = physicsAreaOf(test);
			let expected = {
				x: -105,
				y: -105,
				w: 165,
				h: 165
			}

			assert.deepStrictEqual(actual, expected);
		});

		test('line', () => {
			let test = new Line(new Point(20, 20), new Point(50, 50));

			let actual = physicsAreaOf(test);
			let expected = {
				x: 10,
				y: 10,
				w: 60,
				h: 60
			}

			assert.deepStrictEqual(actual, expected);
		});

		test('rect', () => {
			let test = Rect.fromPosSize(20, 20, 30, 30);

			let actual = physicsAreaOf(test);
			let expected = {
				x: 10,
				y: 10,
				w: 60,
				h: 60
			}

			assert.deepStrictEqual(actual, expected);
		});
	});

	unit('collision.test w/ point', async (test) => {
		let testPoint = new Point(0, 0);

		test('point v ball', () => {
			let ball = new Ball(20, 0, 25);
			let hitOccured = pointBall(testPoint, ball);

			assert.ok(hitOccured);
		});

		test('point v line', () => {
			let line = new Line(
				new Point(-10, 0),
				new Point(10, 0)
			);
			let hitOccured = linePoint(line, testPoint);

			assert.ok(hitOccured);
		});
	});

	unit('collision.test w/ ball', async (test) => {
		let testBall = new Ball(0, 0, 25);

		test('ball v ball', () => {
			let ball = new Ball(20, 0, 25);
			let hitOccured = ballBall(testBall, ball);

			assert.ok(hitOccured);
		});

		test('ball v line', () => {
			let line = new Line(
				new Point(-30, 0),
				new Point(30, 10)
			);
			let hitOccured = lineBall(line, testBall);

			let expected = {
				objects: [line, testBall],
				collisions: [
					new Point(-0.8108108108108105, 4.864864864864865),
					new Point(-24.98602948520363, 0.835661752466061),
					new Point(23.364407863582013, 8.894067977263669),
				],
				hitAngle: 80.53767779197439,
				dot: new Point(-0.8108108108108105, 4.864864864864865),
				entry: new Point(-24.98602948520363, 0.835661752466061),
				exit: new Point(23.364407863582013, 8.894067977263669),
			}

			assert.deepStrictEqual(hitOccured, expected);
		});
	});

	unit('collision.test w/ line', async (test) => {
		let testLine = new Line(
			new Point(-10, 10),
			new Point(10, -10)
		);

		test('line v line', () => {
			let line = new Line(
				new Point(-10, -10),
				new Point(10, 10)
			);

			let hitOccured = lineLine(testLine, line);

			let expected = {
				objects: [testLine, line],
				collisions: [
					new Point(0, 0),
				],
				hitAngle: 45,
				dot: new Point(0, 0),
			}

			assert.deepStrictEqual(hitOccured, expected);
		})
	});

	unit('collision.test w/ rect', async (test) => {
		let testRect = Rect.fromPosSize(-6, -6, 12, 12);

		test('rect v line', () => {
			let line = new Line(
				new Point(-6, -6),
				new Point(6, -6)
			);

			let actual = rectLine(testRect, line);
			let expected = {
				objects: [testRect, line],
				collisions: [
					new Point(6, -6),
					new Point(-6, -6)
				]
			};

			assert.deepStrictEqual(actual, expected);
		});

		test('rect v ball', () => {
			let ball = new Ball(12, 12, 25);

			let actual = rectBall(testRect, ball);
			let expected = {
				objects: [testRect, ball],
				collisions: [
					new Point(-5.349351572897472, -6),
					new Point(6, -6),
					new Point(6, 6),
					new Point(-6, 6),
					new Point(-6, -5.349351572897472),
				]
			};

			assert.deepStrictEqual(actual, expected);
		});
	});
});