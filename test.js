import './physics/point.test.js';
import './physics/line.test.js';
import './physics/ball.test.js';
import './physics/rect.test.js';
import './collision.test.js';